export const Namespace = {
  Group: 'group',
  Project: 'project',
};

export const iterationStates = {
  closed: 'closed',
  upcoming: 'upcoming',
  expired: 'expired',
};

export default {};
